// import { Fragment } from 'react';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';

import AppNavbar from './components/AppNavBar';
import Banner from './components/Banner';
import Highlights from './components/Highlights';

import Products from './pages/Products';
import ProductsView from './pages/ProductView';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import AdminTable from './pages/AdminTable';
import UserTable from './pages/UserTable';

import CreateProduct from './pages/CreateProducts';
import UpdateProduct from './pages/UpdateProducts';
import UserOrder from './pages/UserOrder';
import './App.css';
import { UserProvider } from './UserContext';

function App() {

  const [user, setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
});

  const unsetUser = () => {
    localStorage.clear();
  }



  return (
    
    <UserProvider value={{user, setUser, unsetUser}} >
        <Router>
            <AppNavbar />
            <Container>
                <Routes>
                    <Route path="/" element = {<Home/>} />
                    <Route path="/products" element = {<Products/>} />
                    <Route path="/products/:productId" element = {<ProductsView/>} />
                    <Route path="/register"element = {<Register/>}/>
                    <Route path="/login" element = {<Login/>}/>
                    <Route path="/logout" element = {<Logout/>}/>
                    <Route path="/admintable" element = {<AdminTable/>}/>
                    <Route path="/users" element = {<UserTable/>} />
                    <Route path="/createproduct" element = {<CreateProduct/>}/>
                    <Route path="/userorder" element = {<UserOrder/>}/>
                    <Route path="/updateproduct/:productId" element = {<UpdateProduct/>}/>
                    <Route path="/*" element = {<Error/>}/>
                </Routes>
            </Container>
        </Router>
    </UserProvider>

  );
}

export default App;
