import { Row, Col, Card} from 'react-bootstrap';

export default function Highlights(){
    return (
        <Row className="mt-3 mb-3 justify-content-center">
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Shop from Home</h2>
                        </Card.Title>
                        <Card.Text>
                            Shie Sells offer convenience of being able to shop at any time of the day or night, the ability to compare prices and products easily, and the ability to avoid the crowds and lines that often accompany in-person shopping. 
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Shop Now, Pay Later</h2>
                        </Card.Title>
                        <Card.Text>
                            We offer a shop now pay later option to our customers to provide more flexibility and convenience when shopping. Our shop offers shop now pay later option that allows customers to make purchases without having to pay the full amount upfront.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>

    )
}
