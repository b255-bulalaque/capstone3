import React from 'react';
import {useState, useEffect, useContext} from 'react';
import { Card, Button,Carousel } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import bp from "../assets/bp.png"


export default function ProductCard({productProp}) {
   


    const { name, description, price, _id } = productProp;
   

    return (
         <>
       

        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                
                <Button className="checkoutButton" variant="light" as={Link} to={`/products/${_id}`}>Checkout</Button>
            </Card.Body>
        </Card>
        </>

    )
}

ProductCard.propTypes = {
    // The shape method is usde to check  if a prop object conforms to a specific shape
    product: PropTypes.shape({
        // Define the properties and their expected types
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
    })
}

