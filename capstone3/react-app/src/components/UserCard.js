import React from 'react';
import {useState, useEffect, useContext} from 'react';
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function UserCard({userProp}) {
   


    const { email, _id } = userProp;
   

    return (
        <Card>
            <Card.Body>
                <Card.Title>{Email}</Card.Title>
                
                
                <Button className="adminButton" variant="primary" as={Link} to={`/users/${_id}`}>Set as Admin</Button>
            </Card.Body>
        </Card>


    )
}

UserCard.propTypes = {
    // The shape method is usde to check  if a prop object conforms to a specific shape
        user: PropTypes.shape({
        // Define the properties and their expected types
        email: PropTypes.string.isRequired,
        
    })
}
