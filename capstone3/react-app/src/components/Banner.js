

import { Button, Row, Col } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import shiel from "../assets/shielogo.png";


export default function Banner(){
	return (
		<Row class="row">
				<Col>
				<img src={shiel} />
				</Col>
			    <Col className= "p-5 text-center" >
				    <h1>Shie Sells</h1>
				    <p>Get the best deals on Products at Shie Sells. Browse our extensive selection of Medical Supplies and find exactly what you're looking for at prices you won't find anywhere else. Plus, with free shipping on orders over Manila, shopping at Shie Sells has never been easier. Shop now and save big!</p>
				     <Button variant="light" as={Link} to="/products">Shop Now!</Button>
				</Col>
		</Row>

	)
}