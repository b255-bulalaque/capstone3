
import { useContext } from 'react';
import{Navbar, Nav, Container, NavDropdown} from 'react-bootstrap';
import{ Link } from 'react-router-dom';
import UserContext from '../UserContext';
import shielogo from "../assets/shielogo.png";


export default function AppNavbar(){

	const { user } = useContext(UserContext);
	console.log(user)
	console.log(user.isAdmin)



	return(

		<Navbar bg="light" expand="lg">
		      <Container>
		               		
		        <Navbar.Brand as={Link} to="/"><img src={shielogo} style={{ width: '100px' }} />
				</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="mx-auto">
		            <Nav.Link as={Link} to="/">Home</Nav.Link>
		            
		            <Nav.Link as={Link} to="/products">Products</Nav.Link>

		             
		            
		            	
		            {
		          
		            	(user.id !== null) ? 
		            		 <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
		       

		            		 :
		            		 <>
		            		 	<Nav.Link as={Link} to="/login">Login</Nav.Link>
		            		 	<Nav.Link as={Link} to="/register">Register</Nav.Link>

		            		 </>

		            }

		            {
		            	(user.id === user.isAdmin) ?
		            		
				            <NavDropdown title="Admin Dashboard" id="basic-nav-dropdown">
				                <Nav.Link as={Link} to="/admintable">Product</Nav.Link>
				            <Nav.Link as={Link} to="/users">User</Nav.Link>
		                        </NavDropdown>
		            		:
		            	<>
		            	</>
		            }



		            
		          </Nav>
		        </Navbar.Collapse>
		      </Container>
		    </Navbar>

	)
}
