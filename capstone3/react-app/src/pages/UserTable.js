import { useState, useEffect} from 'react';
import { Row, Col, Card, Button, Table } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import productData from '../data/productData';
import userData from '../data/userData';
import Alert from 'react-bootstrap/Alert';


export default function AdminTable() {
	const [products, setProducts] = useState([]);
	const [users, setUsers] = useState([]);
	const [avail, setAvail] = useState([]);



	

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/allUser`, {
				headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
		}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			setUsers(data) 
			// console.log(products)
		})
	}, [])

	
	const archiveAdmin = (userId) => {
			fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/user`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					isAdmin: false
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				if (data === false){
					Swal.fire({
						title: "Error! Something Wrong.",
						icon: "error"
					})
				} else {
					Swal.fire({
						title: "Admin return as user",
						icon: "success"
					})
				}

				
			})
	}

	

	const activateUser = (userId) => {
		console.log("hey")
		fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/admin`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isAdmin: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data === false){
				Swal.fire({
					title: "Error! Something Wrong.",
					icon: "error"
				})
			} else {
				Swal.fire({
					title: "User is now an Admin!",
					icon: "success"
				})
			}

			
		})
	}

	


	return (


		<>
			<div class = "text-center">
				<Alert  variant="info">
        		<Alert.Heading>Admin Dashboard</Alert.Heading>
        		</Alert>
				
			</div>
	
			<table class="table table-light table-striped my-4">
			<thead>
			    <tr>
			      <th scope="col">Email</th>
			      
			      <th scope="col">Availability</th>
			      <th scope="col">Actions</th>
			    </tr>
			  </thead>
			  
			  <tbody>
			  	{users.map((data) => {
			  		console.log(data._id)
			  		return(
			  			<tr>
			  			  <th scope="row">{data.email}</th>
			  			  
			  			  <td>{data.isAdmin ? "Admin" : "Not Admin"}</td>
			  			  <td>
			  			  	
			  			  	{
			  			  		(data.isAdmin === true) ?
			  			  		<Button onClick={(e) => {archiveAdmin(data._id)}} variant="outline-danger my-1">Disable</Button>
			  			  		:
			  			  		<Button onClick={(e) => {activateUser(data._id)}} variant="outline-primary my-1">Enable</Button>
			  			  	}
			  			  </td>
			  			</tr>			  		
			  		)
			  	})}
			  </tbody>
			</table>
		</>
	)

}

