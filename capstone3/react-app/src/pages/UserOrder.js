import {useEffect, useState, useContext} from "react";
import {Container, Row, Table, Button} from "react-bootstrap";
import Swal from 'sweetalert2';
import {Link} from "react-router-dom";

export default function History() {

	const [products, setProducts] = useState([]);
	const [users, setUsers] = useState([]);
	const [order, setOrders] = useState([])

	// Checks to see if the mock data was captured.
	// console.log(coursesData);
	// console.log(coursesData[0]);


    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/oders/userorder`, {
            headers: {
                "Content-Type":"application/json",
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            setOrders(
                data.map(order => {
                    return (
                        <tr key={order._id}>
                            
                            
                        </tr>
                    )
                })
            )
        })

    }, [order])




	return (
		 <>
            <div className=''>
                <Container className="custom-account-wrapper">
                    <h1 className='text-center mb-5'>All Users Orders</h1>
                    <div >
                        <Table >
                            <thead>
                                <tr>
                                    <th>User ID</th>
                                    <th>Last Name</th>
                                    <th>First Name</th>
                                    <th>Email</th>
                                    <th>Ordered Product</th>
                                       
                                </tr>
                            </thead>
                            <tbody>
                                {order}
                            </tbody>
                        </Table>
                    </div>

                    <Button as={Link}to="/admin" size="sm" variant="success">Back to Admin Dashboard</Button>
                </Container>
            </div>
        </>
    );

};


