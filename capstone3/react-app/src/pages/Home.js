import { Fragment } from 'react';
import Banner from '../components/Banner';
import React from 'react'
import Highlights from '../components/Highlights';
import HomeImage from "../assets/image-stet.jpg"


export default function Home() {
	const data = {
    title: "Shie Sells",
    content: "Shop anytime, everywhere",
    destination: "/products",
    label: "Buy Now!!"
}

return (
    <React.Fragment>
        <div className="home"> 
            <Banner data={data}/>
            <Highlights />
        </div>
    </React.Fragment>
)


}
