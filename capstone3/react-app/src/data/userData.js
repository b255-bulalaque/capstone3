const userData = [
    {
        "_id": "6436933abc5954c19d82b824",
        "email": "storm@mail.com",
        "password": "$2b$10$7gOV397a.hAU.qu.7eWcT.k9v4xlxuKTOhi0JboyBaAh8icrvS3Rm",
        "isAdmin": true,
        "orderedProduct": [],
        "__v": 0
    },
    {
        "_id": "6437e8931bc92cdba6b7acd6",
        "email": "shopee@mail.com",
        "password": "$2b$10$3KYZFqeW5tKTljpP8SID0eObnY16QhcqbYY2dicCM6dIbJdsRdily",
        "isAdmin": true,
        "orderedProduct": [],
        "__v": 0
    },
    {
        "_id": "6437e8a41bc92cdba6b7acd8",
        "email": "tiktokshop@mail.com",
        "password": "$2b$10$1Z9o4RlXqE.L1Tw7DqOt3OXeUjbV6pMHgwRagwltc.Vt83ZNqe0CO",
        "isAdmin": false,
        "orderedProduct": [],
        "__v": 0
    },
    {
        "_id": "6437e8b41bc92cdba6b7acda",
        "email": "lazada@mail.com",
        "password": "$2b$10$ZqPAT7P6Sq6RhGkP8KuZxOCdrmeylfR2QSlBeGHMxW1rNIt5k700u",
        "isAdmin": true,
        "orderedProduct": [],
        "__v": 0
    },
    {
        "_id": "643e719c8e782f9b9bea650d",
        "email": "shein@mail.com",
        "password": "$2b$10$WXCXqJr93WvFRU/KYJzNe.0ePgs.0weDk5K4jlRW44lWszqS6187e",
        "isAdmin": false,
        "orderedProduct": [],
        "__v": 0
    },
    {
        "_id": "643f9efab4013961062f7395",
        "email": "midwife@mail.com",
        "password": "$2b$10$6wEHuWsT1Lg6fgbriixgeuX3FEP7zoF.8jC1EdL/S84Pgepf5VYpG",
        "isAdmin": false,
        "orderedProduct": [],
        "__v": 0
    },
    {
        "_id": "643fc01869769456d415ef10",
        "email": "nurse@mail.com",
        "password": "$2b$10$5wqQX4gPto9ERZYzPwzkPOV6irbkXpytviFLwyoRga8W7FzJ11zIW",
        "isAdmin": false,
        "orderedProduct": [],
        "__v": 0
    },
    {
        "_id": "64589f7859ddd364d51f2868",
        "email": "sunnies@mail.com",
        "password": "$2b$10$QHS5w7UMvzwc7yNfpRsHDeSv9fEy28aiQNMQN5atfUP7zD0da41jS",
        "isAdmin": false,
        "orderedProduct": [],
        "__v": 0
    },
    {
        "_id": "6458a0c17d30506dac056947",
        "email": "thunder@mail.com",
        "password": "$2b$10$q.jvN6rxVzeZLsJqBhtQVOKqNeJ.ma411z25SIcq7a3Rem.8VsTW.",
        "isAdmin": false,
        "orderedProduct": [],
        "__v": 0
    },
    {
        "_id": "6459d2b096b52269ccfe6285",
        "firstName": "fgh",
        "lastName": "fgh",
        "email": "fgh@mail.com",
        "password": "$2b$10$h6nHktx.6R14QtfDW2R.p.zXV.pUzB39aMf1Uf6/DjT7Ra9s./1Da",
        "isAdmin": false,
        "mobileNo": "12345432123",
        "enrollments": [],
        "__v": 0,
        "orderedProduct": []
    },
    {
        "_id": "6459d61ef13289e13b38b792",
        "email": "wer@mail.com",
        "password": "$2b$10$zeC3FjOu/q7K5Daz8OA9Eu3/vrOSGhZwLJCdqmbYoic1pJAy5teBC",
        "isAdmin": false,
        "orderedProduct": [],
        "__v": 0
    },
    {
        "_id": "6459d654f13289e13b38b797",
        "email": "hust@mail.com",
        "password": "$2b$10$G0nlV/CGIeHNkX5bc99ziOFRJHKwWJEyq4sFpMhnTDaQq3Hnx5Yc2",
        "isAdmin": false,
        "orderedProduct": [],
        "__v": 0
    },
    {
        "_id": "6459d747f13289e13b38b7a0",
        "email": "tamad@mail.com",
        "password": "$2b$10$5ONLitCKTa4zqbd4MFZOjuX1B1xW9iz.p94jHpXOiQAdOMXjm0V0m",
        "isAdmin": false,
        "orderedProduct": [],
        "__v": 0
    }
]

export default userData;
