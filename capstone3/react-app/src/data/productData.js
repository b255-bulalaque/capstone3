const productData = [
    {
        "_id": "64369e94fe0c7104892f6239",
        "name": "Electronic Blood Pressure Monitor",
        "description": "Electronic fully automatic digital arm type blood pressure with blood pressure cuff",
        "price": 300,
        "isActive": true
    },
    {
        "_id": "64369f69fe0c7104892f623b",
        "name": "Thermometer",
        "description": "Non contact Thermometer with Fever Alarm",
        "price": 2000,
        "isActive": false,
        
    },
    {
        "_id": "6436a0c8d3d9df894e6a598f",
        "name": "Pluse Oximeter",
        "description": "Portable Fingertip Pulse Oximeter",
        "price": 1500,
        "isActive": true,
        
    },
    {
        "_id": "6437cba5b446ae7973b37fef",
        "name": "First Aide Kit",
        "description": "Medical Supplies for emergency use at home",
        "price": 2000,
        "isActive": true,
      
    },
    {
        "_id": "643d3d9c7dbdb10d756d8646",
        "name": "Face Mask",
        "description": "Non-woven Disposable mask",
        "price": 500,
        "isActive": true,
        
    },
    {
        "_id": "643e61ba2f6148da850e1a9d",
        "name": "Glucometer",
        "description": "Blood glucose meter with free 50 strips",
        "price": 500,
        "isActive": true,
        
    },
    {
        "_id": "643e723a8e782f9b9bea651c",
        "name": "Alcohol",
        "description": "7-% Isoprophyl Alcohol",
        "price": 700,
        "isActive": true,
        
    },
    {
        "_id": "643e7c968e782f9b9bea6541",
        "name": "Alcohol",
        "description": "7-% Isoprophyl Alcohol",
        "price": 700,
        "isActive": true,
      
    },
    {
        "_id": "643fa31c171c6d5729747542",
        "name": "10 cc Syringe",
        "description": "Terumo",
        "price": 50,
        "isActive": true,
        
    },
    {
        "_id": "643fc1686463e249f7e6c2e7",
        "name": "5 cc Syringe",
        "description": "Terumo",
        "price": 30,
        "isActive": true,
       
    }
]

export default productData;
